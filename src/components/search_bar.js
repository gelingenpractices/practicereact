import React, { Component } from 'react';

/*const SearchBar = () => {
    return <input />;
};*/
class SearchBar extends Component {
    constructor(props){
        super(props);

        this.state = {term: ''};
    }

    render() {
        return (
            <div className="search-bar">
                <input 
                value={this.state.term}
                onChange={ event => this.searchInputChanges(event.target.value) } />
            </div>
        );
    }

    searchInputChanges(term){
        this.setState({term});
        this.props.onSearchTermChange(term);
    }
}

//state is an js object used to record and return on action performance by users.

export default SearchBar;