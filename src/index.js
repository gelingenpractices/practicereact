import React, { Component } from 'react';
import _ from 'lodash';
import ReactDOM from 'react-dom';
import SearchBar from './components/search_bar';
import YTSearch from 'youtube-api-search';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail';
const API_KEY = 'AIzaSyB7jWBCaJ2MNkNAYoIwsaKoSg6nd-gWtm8';



//Create a new component. This component should generate some HTML
class App extends Component {
  constructor(props){
    super(props);

    this.state = { 
      videos: [],
      selectedVideo: null 
    };

    this.searchTermChange('micheal phelps');
  }

  searchTermChange(term){
    YTSearch({ key: API_KEY, term: term }, (videos) => { 
      this.setState({ 
        videos : videos,
        selectedVideo: videos[0]
      }) 
    });
  }

  render(){
    const search = _.debounce((term) => { this.searchTermChange(term) }, 300);
    return (
      <div>
        <SearchBar onSearchTermChange={ search } />
        <VideoDetail video={this.state.selectedVideo} />
        <VideoList onVideoSelect={ selectedVideo => this.setState({selectedVideo}) } videos={this.state.videos} />
      </div>
    );
  }
}

ReactDOM.render(<App />, 
document.querySelector(".container"));